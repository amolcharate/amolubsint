package com.ubs.opsit.interviews;


import org.junit.*;


import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
//test main class
public class MainTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void testMain() {

        String[] args = {"12:30:30"};

        Main.main(args);

        String expected = "====== Berlin Clock ======\n"+
                ",Y\n"+
                ",RR00\n"+
                ",RR00\n"+
                ",YYRYYR00000\n"+
                ",0000\n"+
                ",==========================\n,";

       String actual = outContent.toString();
       
        actual = actual.replaceAll("[^a-zA-Z0-9]", "");
        expected = expected.replaceAll("[^a-zA-Z0-9]", "");
        assertEquals(expected, actual);
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

}
