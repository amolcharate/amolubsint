package com.ubs.opsit.interviews;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeConverterTest {

	private String formattedTime;   

	@Test
	public void testprocessTime() {
		
		TimeConverter timeConverter = new TimeConverterImpl();

		int hours = 12;
		int minutes = 30;
		int seconds = 30;
		formattedTime = timeConverter.processTime(hours, minutes, seconds);

	        String expected = "Y\n" +
	                "RR00\n" +
	                "RR00\n" +
	                "YYRYYR00000\n" +
	                "0000\n";

	       
	        
	        formattedTime = formattedTime.replaceAll("[^a-zA-Z0-9]", "");
	        expected = expected.replaceAll("[^a-zA-Z0-9]", "");
	        assertEquals(expected, formattedTime);
	}
	
	
	@Test
	public void testrowString() {
		
		TimeConverter timeConverter = new TimeConverterImpl();

		int lightsOn = 3;
		int lightsInRow = 4;
		String lightType = "R";

		String expStr = "RRR0";

		String actualstr = timeConverter.rowString(lightsOn, lightsInRow, lightType);

		assertEquals(expStr, actualstr);
	}
	
	
	
	
	
	

}
