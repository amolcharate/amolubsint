package com.ubs.opsit.interviews;

public interface TimeConverter {

    String processTime(int hours, int minutes, int seconds);
    String rowString(int lightsOn, int lightsInRow, String lightType);
}
