package com.ubs.opsit.interviews;

import java.util.Arrays;
import java.util.Collections;

public class TimeConverterImpl implements TimeConverter {

	private static final String NEW_LINE = System.getProperty("line.separator");
	
	public String processTime(int hours, int minutes, int seconds) {

		        String line1 = (seconds % 2 == 0) ? "Y" : "0";
		        String line2 = rowString(hours / 5, 4, "R");
		        String line3 = rowString(hours % 5, 4, "R");
		        String line4 = rowString(minutes / 5, 11, "Y").replaceAll("YYY", "YYR");
		        String line5 = rowString(minutes % 5, 4, "Y");
		        
		        return String.join(NEW_LINE, Arrays.asList(line1, line2, line3, line4, line5));

		    }
	
	   /**
     * Creates a string for each row of the berlin clock.
     * @param litLights
     * @param lightsInRow
     * @param lightType
     * @returnn A string representing a single row of the clock.
     */
     public String rowString(int lightsOn, int lightsInRow, String lightType) {

        int lightsOff = lightsInRow - lightsOn;
        String lightOn = String.join("", Collections.nCopies(lightsOn, lightType));
        String lightoff = String.join("", Collections.nCopies(lightsOff, "0"));

        return lightOn + lightoff;
    }
		
	

}
